FROM r-base

RUN apt update &&\
    apt install -y libcurl4-openssl-dev libxml2-dev libssl-dev

RUN Rscript -e 'install.packages(c("thematic", "shiny","tidyverse","reshape2","shinythemes","plotly"))'

RUN mkdir /apps

COPY . /apps

WORKDIR /apps

COPY ./entrypoint.sh /

RUN chmod 755 /entrypoint.sh

ENTRYPOINT /entrypoint.sh
